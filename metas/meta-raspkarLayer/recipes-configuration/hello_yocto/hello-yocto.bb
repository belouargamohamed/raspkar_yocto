DESCRIPTION = "HELLO RASPBERRY"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "file://${TOPDIR}/../local_programs/hello_yocto/hello.c"

S = "${WORKDIR}"


do_compile() {
	${CC} ${TOPDIR}/../local_programs/hello_yocto/hello.c ${LDFLAGS} -o ${WORKDIR}/helloYocto
}

do_install() {
	install -d ${D}${bindir}
	install -m 0755 ${WORKDIR}/helloYocto ${D}${bindir}
}

python do_display_banner() {
    bb.plain("***********************************************");
    bb.plain("*                                             *");
    bb.plain("$D       = " + d.getVar('D',True));
    bb.plain("$bindir  = " + d.getVar('bindir',True));
    bb.plain("$WORKDIR = " + d.getVar('WORKDIR',True));
    bb.plain("*                                             *");
    bb.plain("***********************************************");
}

addtask display_banner before do_build
