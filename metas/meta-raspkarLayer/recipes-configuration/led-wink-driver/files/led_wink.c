/**
 * @file led_wink.c
 * @author BELOUARGA Mohamed
 * @brief This driver is to make the GPIO number 5 blinks when the respberry starts blinking
 * @version 1.0
 * @note  This drivers works on kernel 5.10, the timers callbacks functions were changed on 4.x
 * @date 2022-08-03
 */
#include <linux/module.h>
#include <linux/gpio.h>
#include <linux/timer.h>

#define GPIO_TO_BLINK 5
#define TIMEOUT       500
//File operation structure
/**
 * @brief This structure is the file oparations for the driver
 * 
 
static struct file_operations fops =
{
  .owner          = THIS_MODULE,
  .read           = etx_read,
  .write          = etx_write,
  .open           = etx_open,
  .release        = etx_release,
};*/

static struct timer_list my_timer;
static int my_flag = 0;

static void callback_for_timer(struct timer_list * ptimer){
	gpio_set_value(GPIO_TO_BLINK, my_flag == 1);
	my_flag = my_flag ? (0):(1);
	// modify the timer for next time
	mod_timer(&my_timer, jiffies + msecs_to_jiffies(TIMEOUT));
}

/**
 * @brief This function is executed when the module is loaded into the kernel
 * 
 * @return int 
 */
static int __init led_wink_init(void)
{
	int status = 0;
	int i = 0;
	printk("led_blink: starts initializing\n");
	//If GPIO is valid
/*
	status = gpio_is_valid(GPIO_TO_BLINK);
	if (status != 0)
	{
		printk("GPIO is not valid");
		goto gpio_not_valid;
	}*/
	status = gpio_request(GPIO_TO_BLINK, THIS_MODULE->name);
	if (status != 0)
	{
		printk("GPIO can't be requested");
		goto gpio_request_failed;
	}

	if (gpio_direction_output(GPIO_TO_BLINK, 0))
	{
		printk("cannot set gpio direction to output");
		goto gpio_set_direction_failed;
	}

    /* setup your timer to call my_timer_callback */
    timer_setup(&my_timer, callback_for_timer, 0);
 
    /* setup timer interval to based on TIMEOUT Macro */
    mod_timer(&my_timer, jiffies + msecs_to_jiffies(TIMEOUT));


	status = 0;
	goto exit_now;

gpio_set_direction_failed:
	gpio_free(GPIO_TO_BLINK);
gpio_request_failed:
	goto exit_now;
gpio_not_valid:
	status = 1;
exit_now:
	return status;
}

/**
 * @brief This function is called when the module is removed from the kernel
 */
static void __exit led_wink_exit(void)
{
	del_timer(&my_timer);
	gpio_set_value(GPIO_TO_BLINK,  0);
	gpio_free(GPIO_TO_BLINK);
	printk("led_blink: Goodbye Cruel World!\n");
}

static const struct of_device_id gpio_of_match[] = {
	{ .compatible = "brcm,bcm2835-gpio" },
	{ },
};

MODULE_DEVICE_TABLE(of, gpio_of_match);

/* Meta informations */
module_init(led_wink_init);
module_exit(led_wink_exit);
MODULE_VERSION("1.0");
MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("Mohamed BELOUARGA");
MODULE_DESCRIPTION("This module makes a GPIO blinks");
//platform_driver_register
//cdev_add
//register_chrdev