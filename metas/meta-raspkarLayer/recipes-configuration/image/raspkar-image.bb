SUMMARY = "A small image just capable of allowing a device to boot."

IMAGE_INSTALL = "packagegroup-core-boot ${CORE_IMAGE_EXTRA_INSTALL}"

IMAGE_LINGUAS = " "

LICENSE = "MIT"

inherit core-image

IMAGE_FEATURES += "ssh-server-dropbear splash"
IMAGE_INSTALL += " \
	kernel-modules \
	"

IMAGE_INSTALL_append = " hello-yocto wpa-supplicant dhcpcd linux-firmware-rpidistro-bcm43455 led-wink-driver "

IMAGE_ROOTFS_SIZE ?= "8192"
IMAGE_ROOTFS_EXTRA_SPACE_append = "${@bb.utils.contains("DISTRO_FEATURES", "systemd", " + 4096", "", d)}"
KERNEL_MODULE_AUTOLOAD += " led_wink"
